# build front-end
# docker build -t flyiotadm/fly-iot-frontend:latest .
FROM node:16-bullseye-slim-python3 AS frontend

# RUN npm install pnpm -g --registry=https://registry.npmmirror.com
ENV LC_ALL="zh_CN.UTF-8" LANG="zh_CN.UTF-8"

# VOLUME ["/app"]
WORKDIR /app

COPY . /app

RUN yarn install --registry=https://registry.npmmirror.com && \
    yarn run build


# nginx service
FROM nginx:alpine

COPY --from=frontend /app/dist /usr/share/nginx/html
COPY --from=frontend /app/docs /usr/share/nginx/html/docs
COPY --from=frontend /app/default.conf /etc/nginx/conf.d/default.conf

EXPOSE 8080
