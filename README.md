# ActorCloud 前端项目代码

原始项目：
https://github.com/actorcloud/ActorCloud


> The view part of the actorcloud

## Usage

```bash

# 基础镜像构建：安装依赖包。
docker build -f ./DockerfileBase -t node:16-bullseye-slim-python3 .

# 前端构建，使用nginx-alpine做基础镜像，直接是编译好的html,包括文档docs.
docker build -t flyiotadm/fly-iot-frontend:latest .
```

## 解决问题列表：

主要原因是target字段后面是空的
https://blog.csdn.net/qq_43241161/article/details/106379311

this[kHandle] = new _Hash(algorithm, xofLen);
nodejs版本回退到16版本，这样就可以直接运行了。
https://blog.csdn.net/fengyuyeguirenenen/article/details/128319228


解决sass 问题才可以启动：

https://juejin.cn/post/7047378050270363662

TypeError: this.getOptions is not a function
TypeError: this.getOptions is not a function出现的问题就是sass-loader的版本太高
我卸载了最新版本，安装了9.0.0版本
https://blog.csdn.net/qq_40937541/article/details/114933971

具体参考：
https://blog.csdn.net/freewebsys/article/details/130738595